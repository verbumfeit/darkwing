# darkwing

## What is this?

### darkwing is a frontend starter repo with Typescript, [Next.js](https://nextjs.org/learn), [React](https://reactjs.org/), [TailwindCSS](https://tailwindcss.com/#what-is-tailwind) and [Prettier](https://prettier.io/).

Begin a new frontend project and start creating right away! Next.js offers Server-Side-Rendering and Static-Site-Generation, React and TailwindCSS let you design and create new components quickly.

## What is this not?

> _I am the batteries that are not included!_

This starter was created with quick, mainly static sites in mind. Therefore it does not come bundled with a solution for network requests, authentication/authorization, etc. But you can certainly extend this starter by installing your preferred npm packages with the required functionality.

## Included Extras

[React Icons](https://react-icons.github.io/react-icons/) is included. This allows you to quickly add one of the many icon packages available to your project. This template uses the Sun/Moon icons from [Ionicons](https://react-icons.github.io/react-icons/icons?name=io), but you can use any other icons from React Icons by importing them.

## Recommended VSCode extensions

[Tailwind CSS Intellisense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)

## Deployment

You can deploy darkwing however you like, but, if you don't know where to start, I have a tip for you: I deployed my darkwing site via [Vercel](https://vercel.com/). Hobby Projects are free forever and to deploy your project you only have to link your Github/Gitlab/Bitbucket repo. I only recommend it because I was impressed.

## Suggestions & Development

- Feel free to open issues and PRs if you think something is missing or misconfigured :)
- I will continue to tweak this starter over time, I will keep it lean.
