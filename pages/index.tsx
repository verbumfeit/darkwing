import Head from "next/head"
import Link from "next/link"

import LandingPage from "./landingpage"
import { IoMdSunny, IoMdMoon } from "react-icons/io"

const Index = () => {

  return (
      <div
        className='container bg-white dark:bg-gray-800'>
        <Head>
          <title>darkwing</title>
          <link rel='icon' href='/favicon.ico' />
        </Head>

        <header className='text-gray-600 dark:text-gray-200 text-xs'>
          <Link href='/'>
            <a>darkwing</a>
          </Link>
        </header>

        <main>
          <LandingPage />
        </main>

        <footer></footer>

        <style jsx>{`
          .container {
            min-height: 100vh;
            min-width: 100vw;
            display: grid;
            grid-template-rows: 50px 1fr 50px;
            place-items: center center;
          }

          header {
            display: flex;
            align-items: center;
          }

          main {
          }

          footer {
            width: 30vw;
            height: 50px;
            border-top: 1px solid #eaeaea;
          }
        `}</style>

        <style jsx global>{`
          html,
          body {
            padding: 0;
            margin: 0;

            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
              Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
              sans-serif;
          }
        `}</style>
      </div>
  )
}

export default Index
