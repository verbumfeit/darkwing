import Head from "next/head"
import Link from "next/link"

import LandingPage from "./landingpage"
import React, { useState, useEffect } from "react"
import { IoMdSunny, IoMdMoon } from "react-icons/io"

const defaultTheme = "" // set to either "light" or "dark", defaults to "light", user preference overrides
const ThemeContext = React.createContext(defaultTheme)

const Index = () => {
  const [theme, setTheme] = useState(defaultTheme)

  useEffect(() => {
    const theme =
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches
        ? "dark"
        : defaultTheme || "light"

    setTheme(theme)
  }, [])

  return (
    <ThemeContext.Provider value={theme}>
      <div
        className='container transition-all ease-linear duration-300 bg-background'
        data-theme={theme}>
        <Head>
          <title>darkwing</title>
          <link rel='icon' href='/favicon.ico' />
        </Head>

        <header className='text-gray-600 text-xs'>
          <Link href='/'>
            <a>darkwing</a>
          </Link>
          <button
            className='ml-1'
            onClick={() => setTheme(theme === "light" ? "dark" : "light")}>
            {theme === "light" ? (
              <IoMdMoon className='bg-background' />
            ) : (
              <IoMdSunny className='text-on-background' />
            )}
          </button>
        </header>

        <main>
          <LandingPage />
        </main>

        <footer></footer>

        <style jsx>{`
          .container {
            min-height: 100vh;
            min-width: 100vw;
            display: grid;
            grid-template-rows: 50px 1fr 50px;
            place-items: center center;
          }

          header {
            display: flex;
            align-items: center;
          }

          main {
          }

          footer {
            width: 30vw;
            height: 50px;
            border-top: 1px solid #eaeaea;
          }
        `}</style>

        <style jsx global>{`
          html,
          body {
            padding: 0;
            margin: 0;

            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
              Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
              sans-serif;
          }
        `}</style>
      </div>
    </ThemeContext.Provider>
  )
}

export default Index
